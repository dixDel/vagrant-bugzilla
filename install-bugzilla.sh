#!/usr/bin/env bash
# Setup a Debian/Ubuntu machine for Bugzilla development.
MYSQL_BUGS_PASSWORD="bugzilla"
MYSQL_ROOT_PASSWORD="root"

# Update apt cache only after first boot.
if [ ! -f /var/tmp/apt-updated ]
then
    touch /var/tmp/apt-updated
    apt-get update
fi

# Join the www-data and adm groups to facilitate debugging
adduser vagrant www-data # enable access to bugzilla files
adduser vagrant adm      # enable review of apache logs

# Install all perl packages from apt
# Note: some may be behind the required version, see below.
apt-get -y purge mysql*
apt-get -y autoremove
apt-get -y autoclean
rm -rf /etc/mysql/my.cnf
rm -rf /var/lib/mysql
echo "--- Install MySQL ---"
export DEBIAN_FRONTEND=noninteractive
apt-get install -y mysql-server --fix-missing --fix-broken
mysqladmin -u root password $MYSQL_ROOT_PASSWORD

echo "--- Install apache perl git etc. ---"
apt-get install -y apache2 libappconfig-perl libdate-calc-perl libtemplate-perl libmime-perl build-essential libdatetime-timezone-perl libdatetime-perl libemail-sender-perl libemail-mime-perl libemail-mime-modifier-perl libdbi-perl libdbd-mysql-perl libcgi-pm-perl libmath-random-isaac-perl libmath-random-isaac-xs-perl apache2-mpm-prefork libapache2-mod-perl2 libapache2-mod-perl2-dev libchart-perl libxml-perl libxml-twig-perl perlmagick libgd-graph-perl libtemplate-plugin-gd-perl libsoap-lite-perl libhtml-scrubber-perl libjson-rpc-perl libdaemon-generic-perl libtheschwartz-perl libtest-taint-perl libauthen-radius-perl libfile-slurp-perl libencode-detect-perl libmodule-build-perl libnet-ldap-perl libauthen-sasl-perl libtemplate-perl-doc libfile-mimeinfo-perl libhtml-formattext-withlinks-perl libfile-which-perl libgd-dev libmysqlclient-dev lynx-cur graphviz python-sphinx rst2pdf git-core

# Download Bugzilla
echo "--- Install Bugzilla ---"
mkdir -p /var/www/html
cd /var/www/html
git clone --branch release-5.0-stable https://github.com/bugzilla/bugzilla bugzilla

# Configure MySQL
echo "--- Configure MySQL ---"
sed -i "s/max_allowed_packet.*/max_allowed_packet=100M/" /etc/mysql/my.cnf
sed -i "32ift_min_word_len=2" /etc/mysql/my.cnf
mysql -u root -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE bugs"
mysql -u root -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON bugs.* TO bugs@localhost IDENTIFIED BY '$MYSQL_BUGS_PASSWORD'"
service mysql restart

# Add a bugzilla configuration to Apache and enable necessary modules.
echo "--- Configure Apache ---"
cat >/etc/apache2/sites-available/bugzilla.conf<<EOF
ServerName localhost

<Directory /var/www/html/bugzilla>
  AddHandler cgi-script .cgi
  Options +ExecCGI
  DirectoryIndex index.cgi index.html
  AllowOverride All
</Directory>
EOF
a2ensite bugzilla
a2enmod cgi headers expires rewrite
service apache2 restart

# Create a response file to avoid user input in bugzilla configuration
echo "--- Configure Bugzilla ---"
cat >/tmp/bugzilla.responses<<'EOF'
$answer{'create_htaccess'} = 1;
$answer{'webservergroup'} = 'www-data';
$answer{'use_suexec'} = 0;
$answer{'db_driver'} = 'mysql';
$answer{'db_host'} = 'localhost';
$answer{'db_sock'} = '';
$answer{'db_port'} = 0;
$answer{'db_name'} = 'bugs';
$answer{'db_user'} = 'bugs';
$answer{'db_pass'} = 'bugzilla';
$answer{'db_check'} = 1;
$answer{'db_mysql_ssl_ca_file'} = '';
$answer{'db_mysql_ssl_ca_path'} = '';
$answer{'db_mysql_ssl_client_cert'} = '';
$answer{'db_mysql_ssl_client_key'} = '';
$answer{'index_html'} = 0;
$answer{'cvsbin'} = '';
$answer{'diffpath'} = '.';
$answer{'interdiffbin'} = '';
$answer{'urlbase'} = 'http://localhost:8010/bugzilla/';
$answer{'cookiepath'} = '/bugzilla/';
$answer{'mail_delivery_method'} = 'Test';
$answer{'font_file'} = '';
$answer{'webdotbase'} = '';
$answer{'upgrade_notification'} = 'disabled';
$answer{'ADMIN_EMAIL'} = 'admin@example.com';
$answer{'ADMIN_PASSWORD'} = 'password';
$answer{'ADMIN_REALNAME'} = 'Admin';
$answer{'SMTP_SERVER'} = 'localhost';
$answer{'NO_PAUSE'} = 0;
EOF

# Configure and start bugzilla.
(
  cd /var/www/html/bugzilla
  # NOTE: DateTime::TimeZone requires 1.64 for 5.1-devel but trusty's apt provides 1.63.
  #       Email::Sender requires 1.300011 but trusty's apt provides 1.300010-1
  perl -e 'use DateTime::TimeZone 1.64' 2>/dev/null || perl install-module.pl DateTime::TimeZone
  perl -e 'use Email::Sender 1.300011' 2>/dev/null || perl install-module.pl Email::Sender
  perl -w checksetup.pl /tmp/bugzilla.responses
  echo "Provisioning complete."
  echo "Bugzilla available at http://localhost:8010/bugzilla/"
)

echo "--- Setup cron job ---"
crontab -l | { cat; echo "0 * * * * /vagrant/backup-bugs.sh"; } | crontab -
